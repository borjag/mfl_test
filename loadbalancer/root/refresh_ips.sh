#!/bin/bash
export AWS_DEFAULT_REGION=eu-west-1
rm -rf /tmp/new_ips.txt
rm -rf /etc/haproxy/haproxy.cfg_new
for i in `aws autoscaling describe-auto-scaling-groups --auto-scaling-group-name AppServer_ASG | grep -i instanceid  | awk '{ print $2}' | cut -d',' -f1| sed -e 's/"//g'`
  do
    aws ec2 describe-instances --instance-ids $i | grep -i "PrivateIpAddress\"" | awk '{ print $2 }' | head -1 | cut -d"," -f1 | sed -e 's/"//g' >> /tmp/new_ips.txt
done

MY_IPS=$(cat /tmp/new_ips.txt | sort)

cp /etc/haproxy/haproxy.cfg_original /etc/haproxy/haproxy.cfg_new

for i in $MY_IPS
 do
   echo "   server $(echo $i | md5sum | awk '{print $1}') $i:80 check" >> /etc/haproxy/haproxy.cfg_new 
done

diff /etc/haproxy/haproxy.cfg_new /etc/haproxy/haproxy.cfg

RESULT=$?
if [ $RESULT -eq 0 ]; then
  echo "Nothing to do"
else
  echo "Cheching syntax"
  /usr/sbin/haproxy -c -V -f /etc/haproxy/haproxy.cfg_new
  RESULT=$?
  if [ $RESULT -eq 0 ]; then
    echo "Switching config files and reloading haproxy"
    mv /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg_old
    mv /etc/haproxy/haproxy.cfg_new /etc/haproxy/haproxy.cfg
    systemctl reload haproxy
  else
    echo "Invalid config file"
  fi
fi
